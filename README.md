# nõbr

Système de numération imaginaire.

![nohbr](nohbr.png)

## Principe de base

Il s'agit d'un [système décimal] (base 10).

## Notation

Les symboles utilisés pour écrire ces nombres sont des chiffres allant de zéro à
neuf. Des diacritiques servent à préciser les puissances de la base. Ainsi il
s'agit d'un [système additif]. Par exemple, 2817 se note 2000·800·10·7.

Ci-dessous, un exemple plus complexe. Il s'agit de trois-milliards-six-cent-vingt-neuf-millions-cent-sept-mille-six-cent-trente-huit :

![3 629 107 638](3629107638.png)

Notez que ce système de notation est indifférent au sens d'écriture.

Remarquez aussi l'absence du zéro correspondant aux dizaines de miliers. Le zéro
ne sert en effet que pour la notation de [nombres réels] compris entre zéro et
un.  Par exemple, 0,124 s'écrira 0,1·20·400. En effet, bien que la partie
décimale use des mêmes symboles, leur sens diffère suivant qu'ils représentent
la partie entière d'un nombre ou ses décimales.

![0,124](0.124.png)

## Limites

La représentation de très grands nombres, au-delà du [quadrillard] (10^27)
commence à éprouver les limites d'un tel système.

[système décimal]: https://fr.wikipedia.org/wiki/Système_décimal
[système additif]: https://fr.wikipedia.org/wiki/Notation_additive_(numération)
[nombres réels]: https://fr.wikipedia.org/wiki/Nombre_réel
[quadrillard]: https://fr.wikipedia.org/wiki/Quadrilliard
